# MyLibrary #

This is one of the exam-like projects written while attending CodersLab programming school. MyLibrary is composed of static website (Client) and ASP.NET Core API. Please note: API and static website was not written by me (html css, some of included js files), at that point of time only script.js was written completly by me. Hovewer, the project has been reviewed, and some changes were made in both API and Client pojects before the commit - mostly in the main javascript.

### How do I get set up? ###

* Prerequisites for API: .NET Core 2.0 runtime, Visual Studio,
* Prerequisites for Client: XAMPP or anything able to host files locally - this is due to newest security patches applied in the browsers, without serving the files as webiste all functionalities are broken, 
* Download ZIP or clone repository,
* Open Rest-Solution.sln (API),
* Run API,
* Copy files from Client catalog to the XAMPP htdocs,
* Open XAMPP console and run Apache server,
* In your browser open default localhost address (localhost:80). If website is not shown - please check ports taken by Apache server.
### Additional packages used in the project ###

* [Bootstrap](https://getbootstrap.com/)
* [Bogus](https://github.com/bchavez/Bogus)
* [Entity Framework](https://www.entityframeworktutorial.net/what-is-entityframework.aspx)
* [jQuery](https://jquery.com/)
* [Popper](https://popper.js.org/)
* [Google Fonts](https://fonts.google.com/)
* [Font Awesome](https://fontawesome.com/)