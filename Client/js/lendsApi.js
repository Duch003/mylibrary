const createLendsApi = (domain) => {
    let output = {};
    output.getAllLends = () => {
        return {
            url: domain + "/api/lend",
            type: "GET",
            dataType: "json"
        };
    }
    
    output.createLend = (reader, title) => {
        return {
            url: domain + "/api/lend/",
            data: {
                BookID: selectedBookId,
                ReaderID: selectedReaderId,
                LendDate: getDotNetDatetimeNow(),
                Title: title,
                Name: reader
            },
            type: "POST",
            dataType: "xml"
        };
    }
    
    output.deleteLend = (id) => {
        return {
            url: domain + "/api/lend/" + id,
            type: "DELETE",
            dataType: "xml"
        };
    }

    return output;
}