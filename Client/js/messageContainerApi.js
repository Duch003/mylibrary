const createMessageContainerApi = () => {
    let output = {};
    output.messageContainer = $("#messageContainer");

    output.messageContainerError = (message) => {
        output.messageContainer.text(message);
        output.messageContainer.removeClass("alert alert-success");
        output.messageContainer.removeClass("alert alert-primary");
        output.messageContainer.addClass("alert alert-danger");
    }
    
    output.messageContainerPass = (message) => {
        output.messageContainer.text(message);
        output.messageContainer.removeClass("alert alert-danger");
        output.messageContainer.removeClass("alert alert-primary");
        output.messageContainer.addClass("alert alert-success");
    }
    
    output.messageContainerWelcome = (message) => {
        output.messageContainer.text(message);
        output.messageContainer.removeClass("alert alert-success");
        output.messageContainer.addClass("alert alert-primary");
    }

    return output;
}