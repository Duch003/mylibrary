const createBooksApi = (domain) => {
    let output = {};

    output.getAllBooks = () => {
        return {
            async: true,
            url: domain + "/api/books",
            data: {},
            type: "GET",
            dataType: "json"
        };
    }
    
    output.editBook = (id, title, author) => {
        return {
            url: domain + "/api/books/" + id,
            data: {
                Title: title,
                Author: author
            },
            type: "PUT",
            dataType: "xml" 
        };
    }
    
    output.deleteBook = (id) => {
        return {
            url: domain + "/api/books/" + id,
            type: "DELETE",
            dataType: "xml" 
        }; 
    }
    
    output.createBook = (title, author) => {
        return {
            url: domain + "/api/books/",
            data: {
                Title: title,
                Author: author
            },
            type: "POST",
            dataType: "json" 
        };
    }
    
    output.getBook = (id) => {
        return {
            url: domain + "/api/books/" + id,
            type: "GET",
            dataType: "json" 
        }; 
    }

    return output;
}