const targetDomain = "http://localhost:52492";

const booksApi = createBooksApi(targetDomain);
const readersApi = createReadersApi(targetDomain);
const lendsApi = createLendsApi(targetDomain);
const messageContainerApi = createMessageContainerApi();

//Statyczna wartość oznaczająca że nie wybrano nic lub anulowano wypożycznie
var staticEmpty = "Nie wybrano";
//Zmienne niezbędne do wypożyczenia książki
var selectedReaderId = staticEmpty;
var selectedBookId = staticEmpty;

//#region Local tags
//var messageContainer;

//Books
let booksButtonsDiv;
let booksTbody;
let addEditBookButton;

let authorInput;
let titleInput;

//Readers
let readersButtonsDiv;
let readersTbody;
let readersForm;
let addEditReaderButton;

let nameInput;
let ageInput;

//Lends
let lendsButtonsDiv;
let lendsTbody;
//#endregion

//#region Tools

const getDotNetDatetimeNow = () => {
    var dateTime = new Date();
    var year = dateTime.getFullYear();
    var month = dateTime.getMonth() + 1;
    var day = dateTime.getDate();

    var hour = dateTime.getHours() + 1;
    var minutes = dateTime.getMinutes();
    var seconds = dateTime.getSeconds();

    return `${day}/${month}/${year} ${hour}:${minutes}:${seconds}`;
}

const lendSelectedBook = () => {
    let name;
    let title;
    $.ajax(readersApi.getReader(selectedReaderId)).done((result) => {
        name = result.Name
    }).fail((xhr, stat, err) => {
        messageContainerApi.messageContainerError("Błąd, nie udało się wypożyczyć książki: " + err);
        return;
    });

    $.ajax(booksApi.getBook(selectedBookId)).done((result) => {
        title = result.Title;
    }).fail((xhr, stat, err) => {
        messageContainerApi.messageContainerError("Błąd, nie udało się wypożyczyć książki: " + err);
        return;
    });

    $.ajax(lendsApi.createLend(name, title)).done((result) => {
        prepareLendsList();
        messageContainerApi.messageContainerPass("Wypożyczono książkę " + title);
    }).fail((xhr, stat, err) => {
        if(xhr.status === 409){
            messageContainerApi.messageContainerError("Czytelnik o podanym imieniu i nazwisku już wypożyczył daną książkę");
            return;
        }
        messageContainerApi.messageContainerError("Błąd, nie udało się wypożyczyć książki: " + err);
    });

    selectedBookId = staticEmpty;
    selectedReaderId = staticEmpty;
    addEditReaderButton.removeClass("btn-secondary");
    addEditReaderButton.addClass("btn-success");
    addEditReaderButton.html("<i class=\"fa fa-plus\"></i> Dodaj");
    messageContainerApi.messageContainerPass("Wypożyczono");
}
//#endregion

//#region Events

//Readers
const prepareReaderFormButton = () => {
    //Obsługa dodawania i edycji w jednej formie
    addEditReaderButton = $("#readerSubmitButton");

    $(addEditReaderButton).on("click", (event) => {
        //Zablokowanie przekierowania
        event.preventDefault();
        //Jeżeli wybrano edycję czytelnika
        if($(addEditReaderButton).hasClass("btn-warning") && nameInput.val() && ageInput.val()){
            const bookID = addEditReaderButton.attr("Reader");
            $.ajax(readersApi.editReader(bookID, nameInput.val(), ageInput.val())).done((result) => {
                addEditReaderButton.removeAttr("Reader");
                addEditReaderButton.removeClass("btn-warning");
                addEditReaderButton.addClass("btn-success");
                addEditReaderButton.html("<i class=\"fa fa-plus\"></i> Dodaj");
                nameInput.val("");
                ageInput.val(0);
                messageContainerApi.messageContainerPass("Pomyślnie edytowano czytelnika");
                prepareReadersList();
            }).fail((xhr, status, err) => {
                if(xhr.status === 204){
                    messageContainerApi.messageContainerError("Brak danych do aktualizacji");
                    return;
                }
                if(xhr.status === 404){
                    messageContainerApi.messageContainerError("Nie odnaleziono czytelnika o danym ID w bazie");
                    return;
                }
                messageContainerApi.messageContainerError("Błąd podczas edycji czytelnika: " + err);
            });
        //Jeżeli wybrano dodanie nowego czytelnika
        } else if($(addEditReaderButton).hasClass("btn-success") && nameInput.val() && ageInput.val()){
            $.ajax(readersApi.createReader(nameInput.val(), ageInput.val())).done((result) => {
                nameInput.val("");
                ageInput.val("");
                messageContainerApi.messageContainerPass("Dodano nowego czytelnika");
                prepareReadersList();
            }).fail((xhr, status, err) => {
                if(xhr.status === 409){
                    messageContainerApi.messageContainerError("Czytelnik o podanym imieniu i nazwisku już istnieje w bazie");
                    return;
                }
                messageContainerApi.messageContainerError("Błąd podczas dodawania nowego czytelnika: " + err);
            });
        //Jeżeli użytkownik jest w trakcie wypożyczania książki
        } else if($(addEditReaderButton).hasClass("btn-secondary")){
            selectedBookId = staticEmpty;
            selectedReaderId = staticEmpty;
            addEditReaderButton.removeClass("btn-secondary");
            addEditReaderButton.addClass("btn-success");
            addEditReaderButton.html("<i class=\"fa fa-plus\"></i> Dodaj");
            messageContainerApi.messageContainerPass("Anulowano wypożyczanie");
        //W innym przypadku
        } else {
            messageContainerApi.messageContainerError("Należy podać imię, nazwisko i wiek oraz czytelnik musi mieć conajmniej 13 lat!");
        }
    });
}

const prepareReaderRemoveButton = (button, id) => {
    $(button).attr("book", id);
    $(button).on("click", () => {
        const elementID = $(button).attr("book");
        $.ajax(readersApi.getReader(elementID)).done((result) => {
            bootbox.confirm({
                message: "Czy jesteś pewny że chcesz usunąć czytelnika " + result.Name + "?",
                buttons: {
                    confirm: {
                        label: 'Tak',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'Nie',
                        className: 'btn-danger'
                    }
                },
                callback: (result) => {
                    if(!result) { return; }

                    $.ajax(readersApi.deleteReader(id)).done((message) => {
                        messageContainerApi.messageContainerPass("Usunięto czytelnika");
                        prepareReadersList();
                    }).fail((xhr, stat, err) => {
                        if(xhr.status === 404){
                            messageContainerApi.messageContainerError("Nie istnieje czytelnik o podanym ID");
                            return;
                        }
                        messageContainerApi.messageContainerError("Błąd podczas usuwania czytelnika: " + err);
                    });
                }
            });
        }).fail((xhr,status,err) => {
            messageContainerApi.messageContainerError("Błąd podczas pobierania ID książki: " + err);
        });
    });
}

const prepareReaderEditButton = (button, id) => {
    $(button).attr("Reader", id);
    $(button).on("click", () => {
        const elementID = $(button).attr("Reader");
        $.ajax(readersApi.getReader(elementID)).done((result) => {
            nameInput.val(result.Name);
            ageInput.val(result.Age);
            addEditReaderButton.removeClass("btn-success");
            addEditReaderButton.addClass("btn-warning");
            addEditReaderButton.html("<i class=\"fa fa-circle\"></i> Zapisz zmiany");
            addEditReaderButton.attr("Reader", elementID)
        });
    });
}

const prepareReaderSelectButton = (button, id) => {
    $(button).attr("Reader", id);
    $(button).on("click", () => {
        const elementID = $(button).attr("Reader");
        selectedReaderId = elementID;

        if(selectedReaderId !== staticEmpty && selectedBookId !== staticEmpty){
            lendSelectedBook();
        } else {
            messageContainerApi.messageContainerPass("Wybierz książkę do wypożyczenia");
            addEditReaderButton.removeClass("btn-success");
            addEditReaderButton.addClass("btn-secondary");
            addEditReaderButton.html("<i class=\"fa fa-window-close\"></i> Anuluj wypożyczanie");
        }
    });
}

//Books
const prepareBookFormButton = () => {
    //Obsługa dodawania i edycji w jednej formie
    addEditBookButton = $("#bookSubmitButton");
    $(addEditBookButton).on("click", (event) => {
        //Zablokowanie przekierowania
        event.preventDefault();

        //Jeżeli wybrano edycję książki
        if($(addEditBookButton).hasClass("btn-warning") && authorInput.val() && titleInput.val()){
            const bookID = addEditBookButton.attr("Book");
            $.ajax(booksApi.editBook(bookID, titleInput.val(), authorInput.val())).done((result) => {
                addEditBookButton.removeAttr("Book");
                addEditBookButton.removeClass("btn-warning");
                addEditBookButton.addClass("btn-success");
                addEditBookButton.html("<i class=\"fa fa-plus\"></i> Dodaj");
                authorInput.val("");
                titleInput.val("");
                messageContainerApi.messageContainerPass("Pomyślnie edytowano");
                prepareBookList();
            }).fail((xhr, status, err) => {
                if(xhr.status === 204){
                    messageContainerApi.messageContainerError("Brak anych do aktualizacji");
                    return;
                }
                if(xhr.status === 404){
                    messageContainerApi.messageContainerError("Nie odnaleziono książki o danym ID w bazie");
                    return;
                }
                messageContainerApi.messageContainerError("Błąd podczas edycji książki: " + err);
            });
        //Jeżeli wybrano dodanie nowej książki
        } else if($(addEditBookButton).hasClass("btn-success") && authorInput.val() && titleInput.val()){
            $.ajax(booksApi.createBook(titleInput.val(), authorInput.val())).done((result) => {
                authorInput.val("");
                titleInput.val("");
                messageContainerApi.messageContainerPass("Dodano nową książkę");
                prepareBookList();
            }).fail((xhr, status, err) => {
                if(xhr.status === 409){
                    messageContainerApi.messageContainerError("Książka o padnym tytule i autorze już istnieje w bazie");
                    return;
                }
                messageContainerApi.messageContainerError("Błąd podczas dodawania nowej książki: " + err);
            });
        //W innym przypadku
        } else {
            messageContainerApi.messageContainerError("Pola tytuł i autor nie mogą być puste!");
        }
    });
}

const prepareBookEditButton = (button, id) => {
    $(button).attr("Book", id);
    $(button).on("click", () => {
        const elementID = $(button).attr("book");
        $.ajax(booksApi.getBook(elementID)).done((result) => {
            authorInput.val(result.Author);
            titleInput.val(result.Title);
            addEditBookButton.removeClass("btn-success");
            addEditBookButton.addClass("btn-warning");
            addEditBookButton.html("<i class=\"fa fa-circle\"></i> Zapisz zmiany");
            addEditBookButton.attr("book", elementID)
        });
    });
}

const prepareBookRemoveButton = (button, id) => {
    $(button).attr("Book", id);
    $(button).on("click", () => {
        const elementID = $(button).attr("book");
        $.ajax(booksApi.getBook(elementID)).done((result) => {
            bootbox.confirm({
                message: "Czy jesteś pewny że chcesz usunąć książkę " + result.Title + "?",
                buttons: {
                    confirm: {
                        label: 'Tak',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'Nie',
                        className: 'btn-danger'
                    }
                },
                callback: (result) => {
                    if(!result) { return; }

                    $.ajax(booksApi.deleteBook(id)).done((message) => {
                        messageContainerApi.messageContainerPass("Usunięto książkę");
                        prepareBookList();
                    }).fail((xhr, stat, err) => {
                        if(xhr.status === 404){
                            messageContainerApi.messageContainerError("Nie istnieje książka o podanym ID");
                            return;
                        }
                        messageContainerApi.messageContainerError("Błąd podczas usuwania książki: " + err);
                    });
                }
            });
        }).fail((xhr,status,err) => {
            messageContainerApi.messageContainerError("Błąd podczas pobierania ID książki: " + err);
        });
    });
}

const prepareBookBorrowButton = (button, id) => {
    $(button).attr("book", id);
    $(button).on("click", () => {
        const elementID = $(button).attr("book");
        selectedBookId = elementID;

        if(selectedReaderId !== staticEmpty && selectedBookId !== staticEmpty){
            lendSelectedBook();
        } else {
            messageContainerApi.messageContainerPass("Wybierz użytkownika który wypożycza książkę");
            addEditReaderButton.removeClass("btn-success");
            addEditReaderButton.addClass("btn-secondary");
            addEditReaderButton.html("<i class=\"fa fa-window-close\"></i> Anuluj wypożyczanie");
        }
    });
} 

//Lends
const prepareLendReturnButton = (button, id) => {
    $(button).attr("Lend", id);
    $(button).on("click", () =>{
        const elementID = $(button).attr("Lend");                                                                                                  
        $.ajax(lendsApi.deleteLend(elementID)).done((result) => {
            messageContainerApi.messageContainerPass("Oddano książkę");
            prepareLendsList();
        }).fail((xhr,status,err) => {
            messageContainerApi.messageContainerError("Błąd podczas usuwania wypożyczenia: " + err);
        });
    });
}
//#endregion

$(window).ready(() => {

    //messageContainer = $("#messageContainer");

    booksButtonsDiv = $("#booksManagementButtons").eq(0);
    booksTbody = $("#booksList");

    booksButtonsDiv.parent().parent().remove();

    prepareBookFormButton();

    authorInput = $("#authorInput");
    titleInput = $("#titleInput");

    prepareBookList();

    readersButtonsDiv = $("#readerManagementButtons").eq(0);
    readersTbody = $("#readersList");
    
    readersButtonsDiv.parent().parent().remove();

    prepareReaderFormButton();

    nameInput = $("#nameInput");
    ageInput = $("#ageInput");

    prepareReadersList();

    lendsButtonsDiv = $("#lendsManagementButtons").eq(0);
    lendsTbody = $("#lendsList");

    prepareLendsList();
    
    messageContainerApi.messageContainerWelcome("Witaj w małej bibliotece!");
    
});

function prepareBooksManagementButtons(id){
    const output = $(booksButtonsDiv).clone();

    const edit = $(output).find("button").eq(0);
    $(edit).removeAttr("id");
    prepareBookEditButton(edit, id);

    const remove = $(output).find("button").eq(1);
    $(remove).removeAttr("id");
    prepareBookRemoveButton(remove, id);

    const borrow = $(output).find("button").eq(2);
    $(borrow).removeAttr("id");
    prepareBookBorrowButton(borrow, id);
    return output;
}

const prepareBookList = () => {
    $.ajax(booksApi.getAllBooks()).done((result) => {
        $(booksTbody).empty();
        $(result).each(index => {
            let trElement = $("<tr></tr>");
            const tdTitleElement = $("<td>" + result[index].Title + "</td>");
            const tdAuthorElement = $("<td>" + result[index].Author + "</td>");
            const divWithButtons = prepareBooksManagementButtons(result[index].ID);
            trElement.append(tdTitleElement);
            trElement.append(tdAuthorElement);
            trElement.append(divWithButtons);

            booksTbody.append(trElement);
        });
    }).fail((xhr,status,err) => {
        messageContainerApi.messageContainerError("Błąd podczas pobierania listy książek: " + err);
    });
}

prepareReadersManagementButtons = (id) => {
    const output = $(readersButtonsDiv).clone();

    const edit = $(output).find("button").eq(0);
    $(edit).removeAttr("id");
    prepareReaderEditButton(edit, id);

    const remove = $(output).find("button").eq(1);
    $(remove).removeAttr("id");
    prepareReaderRemoveButton(remove, id);

    const select = $(output).find("button").eq(2);
    $(select).removeAttr("id");
    prepareReaderSelectButton(select, id);

    return output;
}

const prepareReadersList = () => {
    $.ajax(readersApi.getAllReaders()).done((result) => {
        $(readersTbody).empty();
        $(result).each(index => {
            let trElement = $("<tr></tr>");
            const tdNameElement = $("<td>" + result[index].Name + "</td>");
            const tdAgeElement = $("<td>" + result[index].Age + "</td>");
            const divWithButtons = prepareReadersManagementButtons(result[index].ID);
            trElement.append(tdNameElement);
            trElement.append(tdAgeElement);
            trElement.append(divWithButtons);

            readersTbody.append(trElement);
        });
    }).fail((xhr,status,err) => {
        messageContainerApi.messageContainerError("Błąd podczas pobierania listy książek: " + err);
    });
}

const prepareLendsManagementButtons = (id) => {
    const output = $(lendsButtonsDiv).clone();

    const returnBtn = $(output).find("button").eq(0);
    $(returnBtn).removeAttr("id");
    prepareLendReturnButton(returnBtn, id);

    return output;
}

const prepareLendsList = () => {
    $.ajax(lendsApi.getAllLends()).done((result) => {
        $(lendsTbody).empty();
        $(result).each(index => {
            let trElement = $("<tr></tr>");
            const tdNameElement = $("<td>" + result[index].Name + "</td>");
            const tdTitleElement = $("<td>" + result[index].Title + "</td>");
            const tdDateElement = $("<td>" + result[index].LendDate + "</td>");
            const divWithButtons = prepareLendsManagementButtons(result[index].ID);
            trElement.append(tdNameElement);
            trElement.append(tdTitleElement);
            trElement.append(tdDateElement);
            trElement.append(divWithButtons);

            lendsTbody.append(trElement);
        });
    }).fail((xhr,status,err) => {
        messageContainerApi.messageContainerError("Błąd podczas pobierania listy wypożyczeń: " + err);
    });
}