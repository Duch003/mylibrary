const createReadersApi = (domain) => {
    let output = {};
    output.getAllReaders = () => {
        return {
            url: domain + "/api/readers",
            data: {},
            type: "GET",
            dataType: "json"
        };
    }
    
    output.getReader = (id) => {
        return {
            url: domain + "/api/readers/" + id,
            data: {},
            type: "GET",
            dataType: "json"
        };
    }
    
    output.createReader = (name, age) => {
        return {
            url: domain + "/api/readers",
            data: {
                Name: name,
                Age: age
            },
            type: "POST",
            dataType: "json"
        };
    }
    
    output.editReader = (id, name, age) => {
        return {
            url: domain + "/api/readers/" + id,
            data: {
                Name: name,
                Age: age
            },
            type: "PUT",
            dataType: "xml"
        };
    }
    
    output.deleteReader = (id) => {
        return {
            url: domain + "/api/readers/" + id,
            type: "DELETE",
            dataType: "xml"
        };
    }

    return output;
}