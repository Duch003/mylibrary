//Bazowa domena do której są wysyłane zapytania
//let domain = "http://localhost:63389/";

//Statyczna wartość oznaczająca że nie wybrano nic lub anulowano wypożycznie
var staticEmpty = "Nie wybrano";
//Zmienne niezbędne do wypożyczenia książki
var selectedReaderId = staticEmpty;
var selectedBookId = staticEmpty;

//#region Book list queries
function getAllBooks(){
    var output = {
        async: true,
        url: "/api/books",
        data: {},
        type: "GET",
        dataType: "json"
    }; 

    return output;
}

function searchBooks(title) {
    var output = {
        url: "/api/Books?title=" + title,
        type: "GET",
        //dataType: "xml"
    };

    return output;
}

function editBook(id, title, author){
    var output = {
        url: "/api/books/" + id,
        data: {
            Title: title,
            Author: author
        },
        type: "PUT",
        //dataType: "xml" 
    }; 

    return output;
}

function deleteBook(id){
    var output = {
        url: "/api/books/" + id,
        type: "DELETE",
        //dataType: "xml" 
    }; 
    return output;
}

function createBook(title, author){
    var output = {
        url: "/api/books/",
        data: {
            Title: title,
            Author: author
        },
        type: "POST",
        dataType: "json" 
    }; 

    return output;
}

function getBook(id){
    var output = {
        url: "/api/books/" + id,
        type: "GET",
        dataType: "json" 
    }; 
    return output;
}
//#endregion

//#region Readers list queries
function getAllReaders(){
    var output = {
        url: "/api/readers",
        data: {},
        type: "GET",
        dataType: "json"
    }; 

    return output;
}

function getReader(id){
    var output = {
        url: "/api/readers/" + id,
        data: {},
        type: "GET",
        dataType: "json"
    }; 

    return output;
}

function createReader(name, age){
    var output = {
        url: "/api/readers",
        data: {
            Name: name,
            Age: age
        },
        type: "POST",
        dataType: "json"
    }; 

    return output;
}

function editReader(id, name, age){
    var output = {
        url: "/api/readers/" + id,
        data: {
            Name: name,
            Age: age
        },
        type: "PUT",
        //dataType: "xml"
    }; 

    return output;
}

function deleteReader(id){
    var output = {
        url: "/api/readers/" + id,
        type: "DELETE",
        //dataType: "xml"
    }; 

    return output;
}


//#endregion

//#region Lends list queries
function getAllLends(){
    var output = {
        url: "/api/lend",
        type: "GET",
        dataType: "json"
    }; 

    return output;
}

function createLend(reader, title){
    var date = getDotNetDatetimeNow();
    var output = {
        url: "/api/lend/",
        data: {
            BookID: selectedBookId,
            ReaderID: selectedReaderId,
            LendDate: date,
            Title: title,
            Name: reader
        },
        type: "POST",
        ////dataType: "xml"
    }; 

    return output;
}

function deleteLend(id){
    var output = {
        url: "/api/lend/" + id,
        type: "DELETE",
        ////dataType: "xml"
    }; 

    return output;
}
//#endregion

//#region Local tags
var messageContainer;

//Books
var booksButtonsDiv;
var booksTbody;
var addEditBookButton;

var authorInput;
var titleInput;

//Readers
var readersButtonsDiv;
var readersTbody;
var readersForm;
var addEditReaderButton;

var nameInput;
var ageInput;

//Lends
var lendsButtonsDiv;
var lendsTbody;
//#endregion

//#region Tools

function messageContainerError(message){
    messageContainer.text(message);
    messageContainer.removeClass("alert alert-success");
    messageContainer.removeClass("alert alert-primary");
    messageContainer.addClass("alert alert-danger");
}

function messageContainerPass(message){
    messageContainer.text(message);
    messageContainer.removeClass("alert alert-danger");
    messageContainer.removeClass("alert alert-primary");
    messageContainer.addClass("alert alert-success");
}

function messageContainerWelcome(message){
    messageContainer.text(message);
    messageContainer.removeClass("alert alert-success");
    messageContainer.addClass("alert alert-primary");
}

function getDotNetDatetimeNow(){
    var dateTime = new Date();
    var year = dateTime.getFullYear();
    var month = dateTime.getMonth() + 1;
    var day = dateTime.getDate();

    var hour = dateTime.getHours() + 1;
    var minutes = dateTime.getMinutes();
    var seconds = dateTime.getSeconds();

    return `${day}/${month}/${year} ${hour}:${minutes}:${seconds}`;
}

function lendSelectedBook(){
    var name;
    var title;
    $.ajax(getReader(selectedReaderId)).done(function(result){
        name = result.Name
    }).fail(function(xhr, stat, err){
        messageContainerError("Błąd, nie udało się wypożyczyć książki: " + err);
    });

    $.ajax(getBook(selectedBookId)).done(function(result){
        title = result.Title;
    }).fail(function(xhr, stat, err){
        messageContainerError("Błąd, nie udało się wypożyczyć książki: " + err);
    });

    $.ajax(createLend(name, title)).done(function(result){
       
        prepareLendsList();
        messageContainerPass("Wypożyczono książkę " + title);
    }).fail(function(xhr, stat, err){
        if(xhr.status === 409){
            messageContainerError("Czytelnik o podanym imieniu i nazwisku już wypożyczył daną książkę");
            return;
        }
        console.log(xhr);
        console.log(stat);
        console.log(err);
        messageContainerError("Błąd, nie udało się wypożyczyć książki: " + err);
    });
    
    //TODO Wypożyczyć książkę
    selectedBookId = staticEmpty;
    selectedReaderId = staticEmpty;
    addEditReaderButton.removeClass("btn-secondary");
    addEditReaderButton.addClass("btn-success");
    addEditReaderButton.html("<i class=\"fa fa-plus\"></i> Dodaj");
    messageContainerPass("Wypożyczono");
}
//#endregion

//#region Events

//Readers
function prepareReaderFormButton(){
    //Obsługa dodawania i edycji w jednej formie
    addEditReaderButton = $("#readerSubmitButton");
    $(addEditReaderButton).on("click", function(event){
        //Zablokowanie przekierowania
        event.preventDefault();

        //Jeżeli wybrano edycję czytelnika
        if($(this).hasClass("btn-warning") && nameInput.val() && ageInput.val()){
            var bookID = addEditReaderButton.attr("Reader");
            $.ajax(editReader(bookID, nameInput.val(), ageInput.val())).done(function(result){
                addEditReaderButton.removeAttr("Reader");
                addEditReaderButton.removeClass("btn-warning");
                addEditReaderButton.addClass("btn-success");
                addEditReaderButton.html("<i class=\"fa fa-plus\"></i> Dodaj");
                nameInput.val("");
                ageInput.val(0);
                messageContainerPass("Pomyślnie edytowano czytelnika");
                prepareReadersList();
            }).fail(function(xhr, status, err){
                if(xhr.status === 204){
                    messageContainerError("Brak danych do aktualizacji");
                    return;
                }
                if(xhr.status === 404){
                    messageContainerError("Nie odnaleziono czytelnika o danym ID w bazie");
                    return;
                }
                console.log(xhr);
                console.log(status);
                console.log(err);
                messageContainerError("Błąd podczas edycji czytelnika: " + err);
            });
        //Jeżeli wybrano dodanie nowego czytelnika
        } else if($(this).hasClass("btn-success") && nameInput.val() && ageInput.val()){
            $.ajax(createReader(nameInput.val(), ageInput.val())).done(function(result){
                nameInput.val("");
                ageInput.val("");
                messageContainerPass("Dodano nowego czytelnika");
                prepareReadersList();
            }).fail(function(xhr, status, err){
                if(xhr.status === 409){
                    messageContainerError("Czytelnik o podanym imieniu i nazwisku już istnieje w bazie");
                    return;
                }
                console.log(xhr);
                console.log(status);
                console.log(err);
                messageContainerError("Błąd podczas dodawania nowego czytelnika: " + err);
            });
        //Jeżeli użytkownik jest w trakcie wypożyczania książki
        } else if($(this).hasClass("btn-secondary")){
            selectedBookId = staticEmpty;
            selectedReaderId = staticEmpty;
            addEditReaderButton.removeClass("btn-secondary");
            addEditReaderButton.addClass("btn-success");
            addEditReaderButton.html("<i class=\"fa fa-plus\"></i> Dodaj");
            messageContainerPass("Anulowano wypożyczanie");
        //W innym przypadku
        } else {
            messageContainerError("Należy podać imię, nazwisko i wiek oraz czytelnik musi mieć conajmniej 13 lat!");
        }
    });
}

function prepareReaderRemoveButton(button, id){
    $(button).attr("book", id);
    $(button).on("click", function(){
        var elementID = $(this).attr("book");
        $.ajax(getReader(elementID)).done(function(result){
            bootbox.confirm({
                message: "Czy jesteś pewny że chcesz usunąć czytelnika " + result.Name + "?",
                buttons: {
                    confirm: {
                        label: 'Tak',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'Nie',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if(!result) { return; }

                    $.ajax(deleteReader(id)).done(function(message){
                        messageContainerPass("Usunięto czytelnika");
                        prepareBookList();
                    }).fail(function(xhr, stat, err){
                        if(xhr.status === 404){
                            messageContainerError("Nie istnieje czytelnik o podanym ID");
                            return;
                        }
                        console.log(xhr);
                        console.log(stat);
                        console.log(err);
                        messageContainerError("Błąd podczas usuwania czytelnika: " + err);
                    });
                }
            });
        }).fail(function(xhr,status,err){
            console.log("ERROR");
            console.log(xhr);
            console.log(status);
            console.log(err);
            messageContainerError("Błąd podczas pobierania ID książki: " + err);
        });;
    });
}

function prepareReaderEditButton(button, id){
    $(button).attr("Reader", id);
    $(button).on("click", function(){
        var elementID = $(this).attr("Reader");
        $.ajax(getReader(elementID)).done(function(result){
            nameInput.val(result.Name);
            ageInput.val(result.Age);
            addEditReaderButton.removeClass("btn-success");
            addEditReaderButton.addClass("btn-warning");
            addEditReaderButton.html("<i class=\"fa fa-circle\"></i> Zapisz zmiany");
            addEditReaderButton.attr("Reader", elementID)
        });
    });
}

function prepareReaderSelectButton(button, id){
    $(button).attr("Reader", id);
    $(button).on("click", function(){
        var elementID = $(this).attr("Reader");
        selectedReaderId = elementID;

        if(selectedReaderId !== staticEmpty && selectedBookId !== staticEmpty){
            lendSelectedBook();
        } else {
            messageContainerPass("Wybierz książkę do wypożyczenia");
            addEditReaderButton.removeClass("btn-success");
            addEditReaderButton.addClass("btn-secondary");
            addEditReaderButton.html("<i class=\"fa fa-window-close\"></i> Anuluj wypożyczanie");
        }
    });
}

//Books
function prepareBookFormButton(){
    //Obsługa dodawania i edycji w jednej formie
    addEditBookButton = $("#bookSubmitButton");
    $(addEditBookButton).on("click", function(event){
        //Zablokowanie przekierowania
        event.preventDefault();

        //Jeżeli wybrano edycję książki
        if($(this).hasClass("btn-warning") && authorInput.val() && titleInput.val()){
            var bookID = addEditBookButton.attr("Book");
            $.ajax(editBook(bookID, titleInput.val(), authorInput.val())).done(function(result){
                addEditBookButton.removeAttr("Book");
                addEditBookButton.removeClass("btn-warning");
                addEditBookButton.addClass("btn-success");
                addEditBookButton.html("<i class=\"fa fa-plus\"></i> Dodaj");
                authorInput.val("");
                titleInput.val("");
                messageContainerPass("Pomyślnie edytowano");
                prepareBookList();
            }).fail(function(xhr, status, err){
                if(xhr.status === 204){
                    messageContainerError("Brak anych do aktualizacji");
                    return;
                }
                if(xhr.status === 404){
                    messageContainerError("Nie odnaleziono książki o danym ID w bazie");
                    return;
                }
                console.log(xhr);
                console.log(status);
                console.log(err);
                messageContainerError("Błąd podczas edycji książki: " + err);
            });
        //Jeżeli wybrano dodanie nowej książki
        } else if($(this).hasClass("btn-success") && authorInput.val() && titleInput.val()){
            $.ajax(createBook(titleInput.val(), authorInput.val())).done(function(result){
                console.log(result);
                authorInput.val("");
                titleInput.val("");
                messageContainerPass("Dodano nową książkę");
                prepareBookList();
            }).fail(function(xhr, status, err){
                if(xhr.status === 409){
                    messageContainerError("Książka o padnym tytule i autorze już istnieje w bazie");
                    return;
                }
                console.log(xhr);
                console.log(status);
                console.log(err);
                messageContainerError("Błąd podczas dodawania nowej książki: " + err);
            });
        //W innym przypadku
        } else {
            messageContainerError("Pola tytuł i autor nie mogą być puste!");
        }
    });
}

function prepareBookEditButton(button, id){
    $(button).attr("Book", id);
    $(button).on("click", function(){
        var elementID = $(this).attr("book");
        $.ajax(getBook(elementID)).done(function(result){
            authorInput.val(result.Author);
            titleInput.val(result.Title);
            addEditBookButton.removeClass("btn-success");
            addEditBookButton.addClass("btn-warning");
            addEditBookButton.html("<i class=\"fa fa-circle\"></i> Zapisz zmiany");
            addEditBookButton.attr("book", elementID)
        });
    });
}

function prepareBookRemoveButton(button, id){
    $(button).attr("Book", id);
    $(button).on("click", function(){
        var elementID = $(this).attr("book");
        $.ajax(getBook(elementID)).done(function(result){
            bootbox.confirm({
                message: "Czy jesteś pewny że chcesz usunąć książkę " + result.title + "?",
                buttons: {
                    confirm: {
                        label: 'Tak',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'Nie',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if(!result) { return; }

                    $.ajax(deleteBook(id)).done(function(message){
                        messageContainerPass("Usunięto książkę");
                        prepareBookList();
                    }).fail(function(xhr, stat, err){
                        if(xhr.status === 404){
                            messageContainerError("Nie istnieje książka o podanym ID");
                            return;
                        }
                        console.log(xhr);
                        console.log(stat);
                        console.log(err);
                        messageContainerError("Błąd podczas usuwania książki: " + err);
                    });
                }
            });
        }).fail(function(xhr,status,err){
            console.log("ERROR");
            console.log(xhr);
            console.log(status);
            console.log(err);
            messageContainerError("Błąd podczas pobierania ID książki: " + err);
        });
    });
}

function prepareBookBorrowButton(button, id){
    $(button).attr("book", id);
    $(button).on("click", function(){
        var elementID = $(this).attr("book");
        selectedBookId = elementID;

        if(selectedReaderId !== staticEmpty && selectedBookId !== staticEmpty){
            lendSelectedBook();
        } else {
            messageContainerPass("Wybierz użytkownika który wypożycza książkę");
            addEditReaderButton.removeClass("btn-success");
            addEditReaderButton.addClass("btn-secondary");
            addEditReaderButton.html("<i class=\"fa fa-window-close\"></i> Anuluj wypożyczanie");
        }
        // $.ajax(getBook(elementID)).done(function(result){
            
        // }).fail(function(xhr,status,err){
        //     console.log(xhr);
        //     console.log(status);
        //     console.log(err);
        //     messageContainerError("Błąd podczas wypożyczania książki: " + err);
        // });;
    });
} 

//Lends
function prepareLendReturnButton(button, id){
    $(button).attr("Lend", id);
    $(button).on("click", function(){
        var elementID = $(this).attr("Lend");
        console.log("ElementID: " + elementID)                                                                                                        
        $.ajax(deleteLend(elementID)).done(function(result){
            messageContainerPass("Oddano książkę");
            prepareLendsList();
        }).fail(function(xhr,status,err){
            console.log("ERROR");
            console.log(xhr);
            console.log(status);
            console.log(err);
            messageContainerError("Błąd podczas usuwania wypożyczenia: " + err);
        });
        //TODO Zaimplementować oddawanie książek
        // $.ajax(getBook(elementID)).done(function(result){
        //     authorInput.val(result.Author);
        //     titleInput.val(result.Title);
        //     addEditBookButton.removeClass("btn-success");
        //     addEditBookButton.addClass("btn-warning");
        //     addEditBookButton.html("<i class=\"fa fa-circle\"></i> Zapisz zmiany");
        //     addEditBookButton.attr("book", elementID)
        // });
    });
}
//#endregion

var searchControl;
var searchResult;
$(window).ready(function(){

    searchResult = $("#searchResult");

    searchControl = $("#search");
    searchControl.on("input", function () {
        $.ajax(searchBooks($(this).val())).done(function (data) {
            if (searchControl.val() === "") {
                searchResult.slideUp();
                return;
            }
            if (data.length != 0) {
                searchResult.slideDown();
                //searchResult.text(data);
                $(data).each((index, item) => {
                    searchResult.text(searchResult.text() + item.Title);
                });
            }
            else {
                searchResult.slideUp();
            }
            
        });
    });

    console.log(searchControl);
    messageContainer = $("#messageContainer");

    booksButtonsDiv = $("#booksManagementButtons").eq(0);
    booksTbody = $("#booksList");

    booksButtonsDiv.parent().parent().remove();

    prepareBookFormButton();

    authorInput = $("#authorInput");
    titleInput = $("#titleInput");

    prepareBookList();

    readersButtonsDiv = $("#readerManagementButtons").eq(0);
    readersTbody = $("#readersList");
    
    readersButtonsDiv.parent().parent().remove();

    prepareReaderFormButton();

    nameInput = $("#nameInput");
    ageInput = $("#ageInput");

    prepareReadersList();

    lendsButtonsDiv = $("#lendsManagementButtons").eq(0);;
    lendsTbody = $("#lendsList");

    prepareLendsList();
    
    messageContainerWelcome("Witaj w małej bibliotece!");
    
});

function prepareBooksManagementButtons(id){
    var output = $(booksButtonsDiv).clone();

    var edit = $(output).find("button").eq(0);
    $(edit).removeAttr("id");
    prepareBookEditButton(edit, id);

    var remove = $(output).find("button").eq(1);
    $(remove).removeAttr("id");
    prepareBookRemoveButton(remove, id);

    var borrow = $(output).find("button").eq(2);
    $(borrow).removeAttr("id");
    prepareBookBorrowButton(borrow, id);
    return output;
}

function prepareBookList(){
    $.ajax(getAllBooks()).done(function(result){
        $(booksTbody).empty();
        $(result).each(index => {
            var trElement = $("<tr></tr>");
            var tdTitleElement = $("<td>" + result[index].Title + "</td>");
            var tdAuthorElement = $("<td>" + result[index].Author + "</td>");
            var divWithButtons = prepareBooksManagementButtons(result[index].ID);
            trElement.append(tdTitleElement);
            trElement.append(tdAuthorElement);
            trElement.append(divWithButtons);

            booksTbody.append(trElement);

        });
    }).fail(function(xhr,status,err){
        console.log(xhr);
        console.log(status);
        console.log(err);
        messageContainerError("Błąd podczas pobierania listy książek: " + err);
    });
}

function prepareReadersManagementButtons(id){
    var output = $(readersButtonsDiv).clone();

    var edit = $(output).find("button").eq(0);
    $(edit).removeAttr("id");
    prepareReaderEditButton(edit, id);

    var remove = $(output).find("button").eq(1);
    $(remove).removeAttr("id");
    prepareReaderRemoveButton(remove, id);

    var select = $(output).find("button").eq(2);
    $(select).removeAttr("id");
    prepareReaderSelectButton(select, id);

    return output;
}

function prepareReadersList(){
    $.ajax(getAllReaders()).done(function(result){
        $(readersTbody).empty();
        $(result).each(index => {
            var trElement = $("<tr></tr>");
            var tdNameElement = $("<td>" + result[index].Name + "</td>");
            var tdAgeElement = $("<td>" + result[index].Age + "</td>");
            var divWithButtons = prepareReadersManagementButtons(result[index].ID);
            trElement.append(tdNameElement);
            trElement.append(tdAgeElement);
            trElement.append(divWithButtons);

            readersTbody.append(trElement);
            
        });
    }).fail(function(xhr,status,err){
        console.log(xhr);
        console.log(status);
        console.log(err);
        messageContainerError("Błąd podczas pobierania listy książek: " + err);
    });
}

function prepareLendsManagementButtons(id){
    var output = $(lendsButtonsDiv).clone();

    var returnBtn = $(output).find("button").eq(0);
    $(returnBtn).removeAttr("id");
    prepareLendReturnButton(returnBtn, id);

    return output;
}


function prepareLendsList(){
    $.ajax(getAllLends()).done(function(result){
        $(lendsTbody).empty();
        $(result).each(index => {
            var trElement = $("<tr></tr>");
            var tdNameElement = $("<td>" + result[index].Name + "</td>");
            var tdTitleElement = $("<td>" + result[index].Title + "</td>");
            var tdDateElement = $("<td>" + result[index].LendDate + "</td>");
            var divWithButtons = prepareLendsManagementButtons(result[index].ID);
            trElement.append(tdNameElement);
            trElement.append(tdTitleElement);
            trElement.append(tdDateElement);
            trElement.append(divWithButtons);

            lendsTbody.append(trElement);
            
        });
    }).fail(function(xhr,status,err){
        console.log(xhr);
        console.log(status);
        console.log(err);
        messageContainerError("Błąd podczas pobierania listy wypożyczeń: " + err);
    });
}